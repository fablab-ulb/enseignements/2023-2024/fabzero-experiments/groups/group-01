# Groupe et présentation du projet

---

## Qui sommes nous ?

Bonjour! Nous sommes 4 étudiants participant au cours [Fabzero experiments](https://class-website-fablab-ulb-enseignements-2023-2024-c4a40531a3337f.gitlab.io/), et nous avons choisi de former un groupe ensemble pour mener à bien un projet dont nous présenterons tous les détails sur ce site. Avant cela, une petite présentation s'impose :)  

Deux d'entre nous (Charles et Nikita) sont en bioingénieur, tandis que Christopher est en informatique et Mohamed est en polytechnique orientation informatique. La diversité de compétences du groupe nous a beaucoup aidé pendant la réalisation du projet mais nous détaillerons ça ultérieurement.

- [Charles Ackermans](https://charles-ackermans-fablab-ulb-enseignements-2023--714ae4eb50df2d.gitlab.io/)
- [Christopher Bilba](https://christopher-bilba-fablab-ulb-enseignements-2023--fe5d709fbcf23a.gitlab.io/)
- [Mohamed Ouamar](https://mohamed-ouamar-fablab-ulb-enseignements-2023-202-82d0d17c0c3227.gitlab.io/)
- [Nikita Aksakow](https://nikita-aksakow-fablab-ulb-enseignements-2023-202-b21baf10a76066.gitlab.io/)

L'énergie du groupe est cool et nous nous entendons bien. Ce qui nous unit, c'est clairement cette haine que nous avons tous envers l'humidité. Qui n'a jamais été concerné par ce genre de problème ? Cet enjeu a rythmé notre quotidien pendant le 1er quadrimestre de cette année 2023-2024.

## Le projet: Champi-non.

L'humidité c'était trop pour nous, le combat devait avoir lieu. Nous avons mis au point un hygromètre, portatif et à faible coût de fabrication, afin de surveiller le taux d'humidité à peu près partout. L'important, c'est évidemment de vérifier que son habitation est salubre. Quoi de plus important que de prendre soin de sa santé ?

L'enjeu, c'est surtout de conscientiser les habitants aux problèmes dont ils ne mesurent peut-être pas bien l'ampleur. C'est pourquoi nous fournissont avec le détecteur un petit fascicule explicatif pour utiliser l'appareil et interpréter les résultats, afin de pouvoir réagir le mieux possible selon les différentes situations. En une phrase, nous voulons rendre l'humidité visible, elle qui se cache parfois très bien.

Comment en sommes nous arrivés à un prototype aussi beau et aussi convaincant à la fin du projet ? Que s'est-il passé pendant ces réunions passionantes ? Quels ont été les triomphes et les éceuils auquel nous avons dû faire face ? Les réponses à toutes ces excellentes questions se trouvent dans la section "processus" que vous trouverez à la suite de celle-ci.

![Graphical abstract](images/firstname.surname-slide.png)

---
