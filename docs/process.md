# Processus
C'est dans cette section que vous retrouverez toutes les informations concernant le processus par lequel nous sommes passés afin d'arriver à notre prototype final. Nous reprenons ici le choix de la problématique, les premières idées et tous les changement qui ont eu lieu afin d'aboutir à notre **hygromètre**.

---

# 1. Définition de la problématique

Le projet prend ses racines au sein d’un processus frugale permettant de déterminer des problématiques et des solutions autour du thème de l’année, **Rendre visible, l’invisible**. Chaque membre du groupe dût amener un objet (un crayon, une truelle, de l’isolant thermique et un tube de mort-aux-rats) autour duquel nous avons brainstormé pendant une heure avant d’aboutir à différentes problématiques nous tenant à cœur. Celle qui fut retenue lors de ce premier jour est **la mise en lumière des problèmes d’humidités**.
Nikita en avait déjà connu dans son ancienne maison, Mohamed y était encore confronté très récemment, Charles en a parfois dans sa salle de bain et Christopher qui a un pied dans le milieu du bâtiment sait très bien l’ampleur que cela peut avoir. 
Nous nous sentions donc tous concernés de près ou de loin par cette problématique et avions envie de trouver une façon de l’aborder permettant à tout le monde de la comprendre. Cette motivation se traduit au travers du chemin que nous avons emprunté dans notre **Problem Tree**. 

![](./images/problem-tree.png)

À la suite de ce Problem Tree, nous avons identifié plusieurs points importants :

- La méconnaissance du problème dans son aspect technique
- La méconnaissance des répercussions sur la santé
- La complexité d’interprétations des résultats pour quelqu’un qui ne maitrise pas le sujet

## Appui statistique et public visé

Selon [Walstat](https://walstat.iweps.be/walstat-accueil.php), l’organisme wallon pour l’information statistiques s’appuyant sur des données de StatBel et Eurostat, plus de 15% des logements belges étaient touchés par au moins un problème d’humidité en 2018 et cela montait à presque un logement sur trois lorsqu’on se concentrait uniquement sur Bruxelles. Une part non-négligeable de la population peut donc se sentir concernée et potentiellement intéressée par notre projet. 

![](./images/Stat-logement-HR.png)

Nous avons donc décidé de nous concentrer sur les vieux bâtiments bruxellois car une grande partie de ceux-ci sont des bâtiments construits avant les années 80 avec des matériaux (généralement bois et plâtre) et des techniques laissant plus facilement passer et s’installer l’humidité. Sachant que nous vivons tous à Bruxelles ou dans ses environs, cela paraissait aussi logique de commencer par se focaliser sur ces bâtiments là en particulier.

L’humidité est un problème qui prend essentiellement de l’ampleur lorsqu’on y est confronté sur le long terme. Il était donc important d’également appuyer la notion de sédentarité qui ne cesse d’augmenter depuis le COVID avec entre autres le travail à la maison. Selon une [étude de Santé Publique France parue en 2008](https://www.santepubliquefrance.fr/determinants-de-sante/exposition-a-des-substances-chimiques/monoxyde-de-carbone/documents/rapport-synthese/description-du-budget-espace-temps-et-estimation-de-l-exposition-de-la-population-francaise-dans-son-logement), le français moyen passait déjà presque 16 heures en moyenne à la maison. On peut donc facilement comprendre l’ampleur du problème aujourd’hui.

![](./images/taf-maison-fr.png)

## Quelle réponse peut-on apporter à cette problématique à notre échelle ?

L’idée d’un appareil de mesure de l’humidité, peu importe sa forme, nous est immédiatement apparue comme évidente. Ce n’est cependant pas très novateur et n’apporte rien de plus qu’un appareil de mesure classique trouvable sur le marché. 

Il manque donc selon nous une partie « compréhension des problèmes liés à l’humidité » mais également une partie « interprétation des données ». Il faut donc trouver une façon simple et concise d’apporter les informations clés sans pour autant perdre l’intérêt de la personne utilisant l’appareil. 

Nous savons également que nous ne pourrons pas régler le problème d’humidité présent chez l’utilisateur. L’idée serait donc plutôt de faire un appareil à des fins pédagogique et préventives. 

## Idée finale

L’objectif est donc de faire un objet qui permet à la fois une prise de mesure et une interprétations simple des résultats aboutissant au final à une série de mesures préventives que pourrait prendre l’utilisateur. Ce serait donc **une sorte de KIT accompagné d’un fascicule explicatif**. Le kit serait un boitier composé d'un double capteur servant d'hygromètre et humidimètre. Le premier consiste à mesurer le taux d'humidité dans l'air, le second permet de mesurer le taux d'humidité dans un matériau grâce à la conductivité en plantant 2 picots dans celui-ci.


---

# 2. Les contraintes

## Phénomène de la boite noire

En voyant les appareil qui se vendent sur le marché, on retrouve des boitiers qu’on ne peut ouvrir et dont le seul rôle est d’afficher un % d’humidité dans l’air présent autour de soi. Nous aimerions casser ce phénomène en incluant l’utilisateur dans le montage du boitier de A à Z. Cela permettrait au passage de rendre les pièces interchangeables et donc remplaçables si nécessaire.

## Les objectifs de développement durables (ODD)


Notre projet doit s’aligner sur au moins un des 17 ODD défini par l’ONU pour construire un environnement prospère à l’Homme mais également en accord avec la planète. Notre projet tend à répondre aux trois suivants :

Nr.3                       |  Nr.11                    |  Nr.12
:-------------------------:|:-------------------------:|:-------------------------:
![](./images/odd-3.png)    |  ![](./images/odd-11.png) |  ![](./images/odd-12.png)

- **Nr.3 Bonne santé et bien-être** au travers de la préventions vis-à-vis des différentes maladies que causent les problèmes d’humidités 

- **Nr.11 Ville et communautés durables** avec la mise en lumière de possibles problèmes sanitaires et structurelles présent dans les bâtiments

- **Nr.12 Consommation et production responsable** avec la possibilité de potentiellement remplacer n’importe quelle partie du kit. Ce faisant ce dernier n’est pas jeter dès qu’une pièce ne fonctionne plus.

## Mobilité

La prise de mesure doit pouvoir se faire partout dans le bâtiment. Il faut donc rendre notre appareil mobile en faisant principalement attention à sa taille et à son autonomie en terme de batterie.

## S’entourer de mentors 

L’un des objectifs de ce projet est de s’entourer de mentor fiable afin d’avoir premièrement un appui sur certains aspects techniques nécessitant de l’expérience dans le domaine recherché et deuxièmement afin de pouvoir être redirigé vers un cas concret s’il ne se présentait pas dans l’entourage du groupe de projet. 

Dans l’idéal il nous faut plusieurs mentors afin de pouvoir satisfaire chaque facette du projet. Nous avons eu l’occasion de poser quelques questions à [Étienne](https://sciences.brussels/personnes/etienne-toffin/) et son stagiaire Nicolas, tout deux de la Boutique des Sciences, afin de pouvoir tisser quelques toiles à la mi-novembre. Nicolas nous a judicieusement conseillé de contacter la [Féderation Bruxelloise Unie pour le Logement](https://www.febul.be/), dit FéBul, afin de trouver de possible cas concrets de problèmes d’humidité signalé par des locataires. Notre demande, réalisé par Mohamed, de mentor auprès de la FéBul fut redirigé par Rémy Renson (correspondant au sein de l’association) vers la direction de [Réseau Habitat](https://reseauhabitat.be/). 
Ces derniers sont une collaboration de 9 associations citoyennes travaillant au sein de 9 communes bruxelloise et ayant pour but la rénovation et la revitalisation des quartiers au travers de la participation citoyenne. Étienne contacta Bruna Milani au sein de [Habitat et Rénovation](https://www.habitatetrenovation.be/), la branche Ixelles-Etterbeek de Réseau Habitat, mais nous n’avons malheureusement pas eu de suite à cette demande.

Dans un second lieu, Denis, nous a conseillé de contacter [Nicolas de Coster](https://fablab.ulb.be/about/team/#Nicolas_De_Coster). Ce dernier est affecté au développement des stations automatiques de mesure à l’Institut Royal Météorologique (IRM) mais il est également alumni de la Faculté des Sciences et un ancien collaborateur du FabLab ULB lors de la dernière crise de Covid-19. Nous avions dans l’objectif de l’avoir comme mentor pour tout la partie technique entourant le concept d’humidité relative. C’était malheureusement un nouveau coup dans le vent car nous n’avons pas eu de réponse suite au mail de Mohamed. 

Concernant la partie microcontrôleur de notre projet, nous avons la chance de pouvoir être épaulé par [Axel Cornu](https://fablab.ulb.be/about/team/#Axel_Cornu). Electronicien, spécialisé dans la physique expérimentale et la fabrication numérique. C’est l’un des techniciens au Fablab avec qui nous avons déjà eu l’occasion de travailler lors de nombreux workshop. Il nous a très justement conseillé sur de nombreux aspects fondamentaux du projet ce qui nous a permis d’écarter et d’envisager des possibilités en tout genre.

---

# 3. Développement technique 

## Recherches et prototypes en carton

Le 13 et 14 novembre 2023 furent consacrés à un **Hackathon Solution** destiné à aux premières recherches sur le sujet et à la confection d’une série de prototypes en carton, papier maché, bois,.... que nous présentons ensuite aux reste du groupe. Chaque prototype visait à faire connaitre un moyen de mesurer l’humidité dans une pièce. 

1.	Un prototype mettait en avant l’hygromètre, mesurant l’humidité présente dans l’air, et l’humidimètre à picots, mesurant l’humidité au sein d’un matériaux suite au passage d’un courant électrique proportionnel à la résistance et donc la teneur en eau entre deux picots (en haut à gauche sur l'image).

2.	Un second prototype utilise la déformation d’un écrin de cheval. Ce dernier rétrécit lorsqu’il se vide de son humidité et s’allonge lorsqu’il se gorge de l’humidité présente dans l’air. On fixe alors cet écrin à une petite flèche en papier qui bougera en fonction de la tension imposé par l’écrin sec/humide (en bas à gauche sur l'image).

3.	Un dernier prototype se constitue d’un thermomètre classique et d’un thermomètre humide mesurant la température à la surface de l’eau au moment où celle-ci s’évapore. Les deux sont accrochés à une bouteilles que l’on fait tourner pour obtenir la température humide et la température sèche (celle qu’on connait habituellement) de l'air.
On peut à partir de ces deux données, retrouver l’humidité relative de l’air dans un diagramme psychométrique (en haut à droite sur l'image).

![](./images/table-proto-1.png)

## Première réunion avec Denis 

Le 28 novembre fut l’occasion de faire un premier bilan de sorte à voir quelle direction nous étions en train de prendre. Ce fût un peu la douche froide car nous n’avions pas eu le temps de matérialiser nos idées. Nous en étions resté au processus créatifs avec la projection de le mettre en pratique dès que le temps permettrait. Temps que nous n'avions pas à ce stade.

Nous avons décidé de ne pas essayer d’afficher les données sur un petit écran LCD car cela serait complexe, plus couteux et surtout moins facilement remplaçable. Nous avons pensé à un système de LEDs de couleurs à la place, chacune d'entres elles couvrirait l'une des plages de mesure d'humidité afin de donner directement une interprétation du résultat et non de la mesure.

Nous décidons également à ce moment-là d’abandonner l’idée d’un double capteur hygromètre et humidimètre à picots car aucun capteur n'est mis à disposition pour ce dernier. En réaliser un fiable consisterait en un projet d'envergure que nous avons estimé trop complexe dans le cadre de ce projet-ci, nous avons donc décidé de ne garder qu'un

## Premiers prototypages

### Matériel et détails techniques
La première version de notre hygromètre a commencé par la construction du circuit électronique. Nous avons utilisé comme composants un capteur DHT-20, quatre LEDs de couleur pour afficher le résultat, et un raspberry pi RP2040. Les connections entre les différents composants on été faites sur une breadboard, et le tout était alimenté par un ordinateur. Tous les composants électroniques sont issus d'un kit d'électronique loué par Axel Cornu. Le système prend une mesure toute les secondes, et met à jour la LED correspondante à la mesure prise.

### Affichage des résultats
Nous sommes partis du constat que l'humidité relative, d'habitude affichée sur les hygromètres, n'est pas très parlante et nous donne rarement une idée claire de la situation. Nous avons décidé d'informer l'utilisateur par un code couleur (LED) suivant le taux d'humidité ambiante:

* 0-40% : La LED blanche s'allume. L'air est trop sec. Une exposition prolongée à un tel taux d'humidité peut entrainer des problèmes de santé comme une irritation des muqueuses, des yeux, etc. Humidifier les pièces sèches devraient régler ce problème bien qu'il est plutôt rare à Bruxelles dû aux conditions météorologiques.

* 40-60% : La LED verte s'allume. Le taux d'humidité est optimal et vous n'avez rien à faire.

* 60-80% : La LED orange s'allume. L'humidité est légerement au-dessus des recommandations. Rien de très grave, mais une exposition prolongée comme pendant le sommeil peut avoir un impact sur la santé. Les chambres sont donc à surveiller. Il faut cependant s'intéresser au contexte: si vous avez pris une douche ou que vous avez fait une vaisselle dans la pièce avant la mesure, une hausse d'humidité transitoire peut avoir lieu. Si vous faites cette mesure régulièrement au même endroit, il est recommandé d'aérer la pièce de temps en temps.

* 80-100% : La LED rouge s'allume. L'humidité mesurée est critique et il faut réagir aussi vite que possible. Une aération intense ainsi qu'une ventilation sont de mises et il peut être utile d'emprunter un déshumidificateur ou faire appel à un profesionnel si le problème est persistant.

Toutes ces information sont reprises sur le fascicule accompagnant le détecteur. De cette manière les résultats sont "déjà interpretés" et il suffit de lire le fascicule.

## Deuxième réunion et avancée concrète 

La seconde réunion avec Denis pris place deux semaines plus tard. Nous lui avons présenté un circuit électronique constitué de 4 LEDs, d’un hygromètre, d’un petit buzzer et d’un microcontrôleur (le code Arduino se trouve juste en dessous). Le but de ce circuit est de mesurer l’humidité à intervalles réguliers. L’une des LEDs s’allume alors selon les 4 plages d'humidité présentées au point précédent. Les deux dernières plages sonnent graduellement grâce au petit buzzer de sorte à notifier l’utilisateur. Ces plages ont été déterminées selon les « lignes directrices relatives à la qualité de l’air à l’intérieur des habitations » publiées par [l’Organisation Mondiale de la Santé (OMS)](https://iris.who.int/bitstream/handle/10665/349777/WHO-EURO-2009-4256-44019-62081-fre.pdf?sequence=1) et à divers articles tels que celui de la [RTBF](https://www.rtbf.be/article/le-taux-dhumidite-de-votre-interieur-impacte-directement-votre-sante-10589539).

Les retours sont meilleurs mais il reste encore la mise au propre de toute la partie interprétation des données grâce aux différentes couleurs de LEDs car nos recherches sont encore trop éparpillées et Denis souligne encore le brouillard autour des actions claires à prendre en cas de problèmes d’humidité.

![](./images/circuit-1.png)

```
// Code Arduino utilisé pour notre circuit

#include <iostream>
#include "DHT20.h"
#include "pico/stdlib.h"
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

DHT20 DHT(&Wire);
int White = 1;
int Green = 5;
int Yellow = 9;
int Red = 13; 
int Buzzer = 12;


void setup() { 
  Serial.begin(115200);
  Serial.println("Humidity, Temperature");
  pinMode(Red, OUTPUT);
  pinMode(Yellow, OUTPUT);  
  pinMode(Green, OUTPUT);
  pinMode(Buzzer, OUTPUT);

  Wire.setSDA(16);
  Wire.setSCL(17);
  Wire.begin();

  Serial.begin(115200);
  Serial.println("Humidity, Temperature");
} 


void loop() { 
  DHT.read();

  Serial.print(DHT.getHumidity(), 1);
  Serial.print(", ");
  Serial.println(DHT.getTemperature(), 1);
  std::cout << DHT.getHumidity() << "     " << DHT.getTemperature() << std::endl;

  if (DHT.getHumidity() > 60){
    digitalWrite(Red, LOW); 
    digitalWrite(Yellow, HIGH); 
    digitalWrite(Green, LOW); 
    digitalWrite(White, LOW);
    digitalWrite(Buzzer, HIGH);
    if (DHT.getHumidity() > 80){
      digitalWrite(Red, HIGH); 
      delay(500);
    }
    else {
      delay(150);
    }
    digitalWrite(Buzzer, LOW);
  }

  else if (DHT.getHumidity() < 40){
    digitalWrite(Red, LOW); 
    digitalWrite(Yellow, LOW); 
    digitalWrite(Green, LOW); 
    digitalWrite(White, HIGH);
  }

  else {
    digitalWrite(Red, LOW); 
    digitalWrite(Yellow, LOW); 
    digitalWrite(Green, HIGH); 
    digitalWrite(White, LOW);
  }

  delay(800);
}
```

Voici également le circuit (à noter qu'ici la carte présentée est un Arduino UNO et qu'un câble GND à été triplé à cause du manque de ports mais le fonctionnement reste identique):

![](./images/Circuit-2.PNG)

## Mise en place pré-hackathon 

Avant la dernière ligne droite du hackathon de fin d’année, nous nous sommes réunis sur Discord afin de déterminer la direction que nous allions prendre. Le projet en était encore à un stade de circuit électrique et de recherches complétements azimut. 
Nous décidons alors de dévier de notre idée de « boite » à monter sois même afin de rendre le kit un peu plus design sans pour autant perdre le coté déconstruction d’une boîte noire. 
Nous imaginons alors ce circuit au sein d’un solide en forme de goutte dont l’une des faces serait en plexiglass permettant ainsi de voir le circuit (et donc les LEDs) à l’intérieur. Les faces extérieur (cf le croquis) serait perforés de sorte à avoir une aération permettant premièrement d’éviter la surchauffe du circuit (même si cette dernière est peu probable) et deuxièmement de mesurer l’air extérieur à la goutte (ce qui reste quand même l’objectif initial). La goutte serait donc entreposable sur une étagère, une bibliothèque,... tout en étant déplaçable facilement dans la maison.

![](./images/croquis-goutte.png)

Pour que cette dernière soit mobile, il faut déterminer le type d’alimentation qui doit être d'au moins 4 V. Nous avons alors trois options qui s’offrent à nous :

-	Des batteries rechargeables de récupération
-	Des piles rechargeables (ou non)
-	Brancher notre appareil sur secteur 

L’option qui nous satisfait le plus est l’idée d’une batterie rechargeable.

## Hackathon et pré-jury

Nous démarrons l’hackathon du 21 et 22 décembre avec les points suivants à terminer :

-	Imprimer en 3D notre modèle de goutte en déterminant au préalable la taille idéale par paramétrisation 
-	Découper une plaque type « plexiglass » à la découpeuse 3D une fois que la taille de la goutte est fixée
-	Déterminer quelle batterie utiliser et utiliser des modèles Printables pour trouver un système d’attache
-	Trouver et imprimer un système d’attache pour notre board Raspberry PI
-	Confectionner un fascicule d’utilisation mais surtout d’interprétation avec les recherches réalisées
-	Faire un Powerpoint afin de présenter notre projet
-	Faire un logo pour notre projet si le temps le permet

Nous arrivons à la fin du hackathon en ayant réalisé l’essentiel des point sus-jacents. Nous aboutissons à un boitier non-perforé avec une fine plaque en plexi, un fascicule pliable avec une identité graphique propre à notre projet, un Powerpoint accrocheur et un modèle d’attache pour la board. Nous avons cependant dû nous rabattre sur un modèle de pile à 4,5 V de chez Brico afin de pouvoir tester la mobilité de notre boitier.

![](./images/screen-boitier.png)
![](./images/page-2-fascicule.png)

Le pré-jury permis de faire ressortir les points suivants à retravailler :

-	Sortir le capteur du boitier pour éviter de devoir le perforé, avoir une mesure optimale et pour un remplacement plus rapide en cas de casse. Les capteurs utilisés ont malheureusement une durabilité de quelques mois grand maximums
-	Utiliser une LED RGB au lieu d’en avoir une pour chaque couleur
-	Comparer Raspberry PI et Arduino UNO en terme de consommation énergétique. Un système de On/Off ou de veille pourrait déjà diminuer cette consommation.
-	Mieux aborder l’utilité du fascicule et les risques de l’humidité dans le Powerpoint


## Prototype Final

En prenant en compte les diverses remarques du jury nous avons retravaillé notre prototype. Le fascicule et le PowerPoint de présentation ont été retravaillés ainsi que le boitier/circuit.  

Afin de compacter le boitier qui laissait beaucoup d'espace vide nous avons décide d'en faire un beaucoup plus petit en empilant le Raspberry et la pile, et en préparant un emplacement afin qu'ils ne bougent pas dans le boitier. Les 4 LEDs ont été remplacées par une LED unique RGB, et elle a été déportée tout comme le buzzer actif et le DHT20 afin de permettre un accès et un remplacement facile. Ceci nous a amené à ce prototype final:

Batterie placée            |  Circuit final                    
:-------------------------:|:-------------------------:
![](./images/Final1.jpg)   |  ![](./images/Final2.jpg)

Le code a également été mis à jour:

```
#include <iostream>
#include "DHT20.h"
#include "pico/stdlib.h"
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h> 
#endif

DHT20 DHT(&Wire);
const int alim = 3;
const int redPin = 4;
const int greenPin = 5;
const int bluePin = 6;
int Buzzer = 18;


void setup() { 
  Serial.begin(115200);
  Serial.println("Humidity, Temperature");
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  pinMode(alim, OUTPUT);

  digitalWrite(alim, HIGH);

  pinMode(Buzzer, OUTPUT);

  Wire.setSDA(16);
  Wire.setSCL(17);
  Wire.begin();

  Serial.begin(115200);
  Serial.println("Humidity, Temperature");
} 


void loop() { 
  DHT.read();

  Serial.print(DHT.getHumidity(), 1);
  Serial.print(", ");
  Serial.println(DHT.getTemperature(), 1);
  std::cout << DHT.getHumidity() << "     " << DHT.getTemperature() << std::endl;

  if (DHT.getHumidity() > 60){
    digitalWrite(redPin, LOW); 
    digitalWrite(bluePin, HIGH); 
    digitalWrite(greenPin, LOW); 
    digitalWrite(Buzzer, HIGH);
    if (DHT.getHumidity() > 80){
      digitalWrite(greenPin, HIGH); 
      delay(500);
    }
    else {
      delay(150);
    }
    digitalWrite(Buzzer, LOW);
  }

  else if (DHT.getHumidity() < 40){
    digitalWrite(redPin, HIGH); 
    digitalWrite(bluePin, HIGH); 
    digitalWrite(greenPin, LOW); 
  }

  else {
    digitalWrite(redPin, HIGH); 
    digitalWrite(bluePin, LOW); 
    digitalWrite(greenPin, HIGH); 
  }

  delay(800);
}
```


### Liste des composants

Au final afin de réaliser ce prototype nous avons eu besoin de ces composants:

| Composant     | Prix        |
| -----------   | ----------- |
| RP2040        | 4,5€        |
| DHT20         | 2,8€        |
| Buzzer Actif  | 0,45€       |
| LED RGB       | 0,05€       |
| Câbles        | 0,5€        |
| Impression 3D | 1,3€        |
|               |             |
| TOTAL         | 8,6€        |

Nous sommes arrivés à un prix de production (en tant que particuliers) qui est globalement inférieur au prix d'achat d'un hygromètre entrée de gamme tout en apportant le coté pédagogique et un bon indice de réparabilité.

---

# 4. Dynamique de groupe 

Nous avons très rapidement mis en place une charte à respecter afin d’optimiser notre façon et notre ambiance de travail. Nous avons eu la chance de très bien nous trouver car nous étions tous conscient qu’il ne fallait pas avoir les mêmes attentes vis-à-vis de chaque membre du groupe.  Étant tous plus ou moins spécialisé dans certaines choses, il était plus naturel pour nous de tirer parti de ces points forts plutôt que de chercher à tout prix une charge paritaire du travail pour chaque étape du projet.

Il y avait un respect mutuel au sein du groupe offrant la possibilité de ne pas devoir instaurer des règles pour contenir la prise de parole ou le caractère de certain membre. Les choses suivaient naturellement leur cours.

![](./images/charte-group.png)

Nous sommes tous les quatre dans des études qui nécessitent une grosse charge de travail hebdomadaire et avons donc très rapidement accepté qu’il était nécessaire d'avoir un moyen de communication simple permettant de déterminer et optimiser les sessions de travail. C’est donc sur un serveur Discord que le groupe s’est penché. 
Cette charge de travail était également un frein pour se retrouver et avancer toutes les semaines d’où le fait que nous avons accumulé hebdomadairement une sommes d’idées et de recherches en alimentant les différentes rubriques de notre Discord jusqu’à l’hackathon avant de pouvoir les concrétiser.

Mi-décembre nous avons eu la mauvaise surprise d’apprendre que Mohamed était malheureusement touché par une infection des voies pulmonaires et qu’il n’allait pas pouvoir être présent durant l’hackathon. La santé avant tout, nous avons travaillé en conséquences en utilisant cette mauvaise nouvelle comme une force afin de mettre les bouchées doubles. 

La préparation et la réalisation des examens de janvier se mettant en travers de notre chemin, nous nous sommes mis d’accord pour travailler un maximum sur le projet les quelques jours avant le jury final ce qui a conduit au prototype présenté et à la documentation que vous venez de lire.

---
